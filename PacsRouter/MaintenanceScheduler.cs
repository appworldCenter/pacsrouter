﻿using System;
using System.Linq;
using System.Threading;
using static PACSRouter.PACSRouter;
using static PACSRouter.Utils;

namespace PACSRouter
{
    internal class MaintenanceScheduler
    {
        #region Internal variables     
        Timer timer;
        int maintenanceFrequency;
        #endregion

        internal void Start()
        {
            //  How frequently do we run the maintenance jobs            
            maintenanceFrequency = InitConfig.MaintenanceTaskFrequency * 1000 * 60; // value in minutes

            timer = new Timer(MaintenanceRoutine, null, 0, maintenanceFrequency);
            Log($"Maintenance thread started at {DateTime.Now}");
        }

        //  Maintenance code starts from here
        internal void MaintenanceRoutine(object o)
        {
            try
            {
                var startTime = new TimeSpan(21, 0, 0);                 //  9 pm
                var endTime = new TimeSpan(5, 0, 0);                    //  5 am
                var currentTime = DateTime.Now.TimeOfDay;

                //  Skip if busy hours
                if (currentTime.Ticks <= startTime.Ticks && currentTime.Ticks >= endTime.Ticks)
                {
                    //DebugLog($"Working hours, skip maintenance tasks");
                    return;
                }

                DebugLog($"Running Maintenance tasks");
                bool dbUpdateRequired = false;

                using (var db = GetDB())
                {
                    //  #1  Purge old records
                    try
                    {
                        DateTime startingValidDate = DateTime.UtcNow.AddDays(db.Configurations.First().CacheDays * -1);

                        foreach (var expiredStudy in db.Studies.Where(x => x.DateReceived < startingValidDate))
                        {
                            var related_Series = db.Series.Where(x => x.Study.StudyUID == expiredStudy.StudyUID);
                            foreach (var expiredSeries in related_Series.Where(x => x.DateReceived < startingValidDate))
                            {
                                var related_Instances = db.Instances.Where(x => x.Series.SeriesUID == expiredSeries.SeriesUID);
                                foreach (var expiredInstance in related_Instances.Where(x => x.DateReceived < startingValidDate))
                                {
                                    DebugLog($"Instance record with ID: {expiredInstance.ID}, RemoteAE ID: {expiredInstance.RemoteAEID}, SendStatus: {expiredInstance.SendStatus} deleted");
                                    db.Instances.Remove(expiredInstance);
                                    dbUpdateRequired = true;
                                }

                                if (related_Instances.All(x => x.DateReceived < startingValidDate))
                                {
                                    DebugLog($"Series record with SeriesUID: {expiredSeries.SeriesUID}, Modality: {expiredSeries.Modality} deleted");
                                    db.Series.Remove(expiredSeries);
                                    dbUpdateRequired = true;
                                }
                            }

                            if (related_Series.All(x => x.DateReceived < startingValidDate))
                            {
                                DebugLog($"Study record with AccessionNumber: {expiredStudy.AccessionNumber}, StudyUID: {expiredStudy.StudyUID} deleted");
                                db.Studies.Remove(expiredStudy);
                                dbUpdateRequired = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        FormatExceptionLog(ex, "Purging cache");
                        //  Swallow exception and carry on
                    }

                    if (dbUpdateRequired)
                        db.SaveChanges();

                    db.Database.Connection.Close();     //  Explicitly closing to try to free up SQL connection pool

                    //  #2  Remove orphan records

                    //  #3  Compress images on disk (if not already compressed) and update records
                }
            }
            catch (Exception ex)
            {
                FormatExceptionLog(ex, "MaintenanceRoutine");
                //  Swallow exception and carry on
            }
            finally
            {
                GC.Collect();                            //  Force clean up of unused memory
            }
        }
    }
}