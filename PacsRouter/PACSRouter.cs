﻿using DicomObjects;
using DicomObjects.DicomUIDs;
using PACSRouter.Properties;
using System.Collections.Generic;
using System.Linq;
using static PACSRouter.Utils;

namespace PACSRouter
{
    partial class PACSRouter
    {
        #region Variables
        DicomServer receiver;
        Sender sender;
        MaintenanceScheduler mntShdlr;

        // DB initialized variables
        internal static Configuration InitConfig;
        internal static string[] PreferredTSs;
        internal static List<string> PACSPreferredTSs = new List<string>
        {
                    TransferSyntaxes.JPEG2000Lossless,
                    TransferSyntaxes.JPEGLossless,
                    TransferSyntaxes.JPEGLosslessFirstOrder,
                    TransferSyntaxes.ExplicitVRLittleEndian,
                    TransferSyntaxes.ImplicitVRLittleEndian
                };

        //  Routing Audit
        internal static Routing InstanceRouting;
        #endregion

        #region Service start/stop
        internal void Start()
        {
            //  Start logging
            if (!string.IsNullOrWhiteSpace(ProjectSettings.LogLocation))
            {
                DebugLog($"Starting log: '{ProjectSettings.LogLocation}'");
                DicomGlobal.LogToFile(ProjectSettings.LogLocation, ProjectSettings.LogLevel, 60);
            }

            //  Set up server
            receiver = new DicomServer();
            receiver.VerifyReceived += (s, e) => { e.Status = 0; };
            receiver.AssociationRequest += AssociationRequest;
            receiver.InstanceReceived += InstanceReceived;
            receiver.AssociationClosed += AssociationClosed;

            //  Initialize from DB
            using (var db = GetDB())
            {
                var listeningPorts = db.ListeningPorts.ToArray();
                InitConfig = db.Configurations.First();
                PreferredTSs = db.PreferredTSs.Select(x => x.TransferSyntax).ToArray();

                //  Object to manage routing
                InstanceRouting = new Routing();

                S3StorageService.BucketName = Settings.Default.BucketName;
                S3StorageService.ImageFoldername = InitConfig.StorageRootDirectory.Replace(@"\", "/");

                //  Start listening on ports specified
                foreach (ListeningPort port in listeningPorts)
                    receiver.Listen(port.PortNumber);

                DebugLog($"\tListening on: {string.Join(", ", listeningPorts.Select(x => x.PortNumber))}");

                db.Database.Connection.Close();     //  Explicitly closing to try to free up SQL connection pool
            }

            //  Runs on a background thread to perform sender tasks
            sender = new Sender();
            sender.Start();

            //  Runs on a background thread to perform clean up, retries and other maintenance tasks
            mntShdlr = new MaintenanceScheduler();
            mntShdlr.Start();
        }

        internal void Stop()
        {
            DebugLog("Program stopping");

            receiver?.UnlistenAll();
            receiver = null;
            sender = null;
            mntShdlr = null;
        }
        #endregion
    }
}
