﻿namespace PACSRouter
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PACSRouterProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.PACSRouterInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // PACSRouterProcessInstaller
            // 
            this.PACSRouterProcessInstaller.Password = null;
            this.PACSRouterProcessInstaller.Username = null;
            // 
            // PACSRouterInstaller
            // 
            this.PACSRouterInstaller.Description = "PACSRouter";
            this.PACSRouterInstaller.DisplayName = "PACSRouter";
            this.PACSRouterInstaller.ServiceName = "PACSRouter";
            this.PACSRouterInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.PACSRouterProcessInstaller,
            this.PACSRouterInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller PACSRouterProcessInstaller;
        private System.ServiceProcess.ServiceInstaller PACSRouterInstaller;
    }
}