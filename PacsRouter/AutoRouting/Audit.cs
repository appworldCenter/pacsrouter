﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using static PACSRouter.Utils;

namespace PACSRouter
{
    class Audit
    {
        //      PACS equivalent code to create Audit log in Tran_audit table
        internal static void AuditStudyRouting(object o)
        {
            try
            {
                bool dbUpdateRequired = false;
                using (var db = GetDB())
                {
                    var studiesToAudit = db.RoutingStatuses.Where(x => !x.Audited || (x.Audited && x.SendStatus == SendStatus.Failed && x.Study.SendStatus == SendStatus.Sent));
                    if (studiesToAudit.Any())
                    {
                        //DebugLog($"Routing status update for {studiesToAudit.Count()} studies");

                        using (var MysqlDb = GetMySQLDB())
                        {
                            //  Group individual Remote AE
                            foreach (var studiesToAudit_GroupedAE in studiesToAudit.GroupBy(x => x.RemoteNode))
                            {
                                RemoteAE remoteAE = studiesToAudit_GroupedAE.Key;
                                //  Group by Study
                                foreach (var studyStatusGroup in studiesToAudit_GroupedAE.GroupBy(x => x.Study))
                                {
                                    //  Result status of the Study, this holds individual series' status
                                    var relatedInstancesStatuses = new List<KeyValuePair<SendStatus, string>>();

                                    //  Group by Series
                                    foreach (var seriesStatusGroup in studyStatusGroup.GroupBy(x => x.Series))
                                    {
                                        //  Check all related series status
                                        foreach (var seriesStatus in seriesStatusGroup)
                                        {
                                            //  Cross matching Study->Series grouped by Remote AE == Instances grouped by Series->Remote AE
                                            var instancesForSeriesToAudit = db.Instances.Select(x => new { x.ID, x.Series, x.SendStatus, x.RemoteAEID, x.ErrorComment })
                                                .Where(x => x.Series.SeriesUID == seriesStatus.Series.SeriesUID && x.RemoteAEID == remoteAE.ID);

                                            //  List out series and its related instances with status for generating Audit log
                                            var instances = instancesForSeriesToAudit.Select(x => new { x.ID, x.SendStatus, x.ErrorComment }).ToList();
                                            relatedInstancesStatuses.AddRange(instances.Select(x => new KeyValuePair<SendStatus, string>(x.SendStatus, x.ErrorComment)));

                                            //  Saving study status
                                            seriesStatus.SendStatus = relatedInstancesStatuses.All(x => x.Key == SendStatus.Sent) ? SendStatus.Sent : SendStatus.Failed;     //  All sent = Success or Failure
                                            seriesStatus.Audited = true;
                                        }
                                    }

                                    //  Insert Audit Log to other DB                                            
                                    LogToTran_Audit(studyStatusGroup.Key, remoteAE, relatedInstancesStatuses, MysqlDb);
                                }
                            }
                        }
                        dbUpdateRequired = true;
                    }

                    if (dbUpdateRequired)
                        db.SaveChanges();

                    db.Database.Connection.Close();     //  Explicitly closing to try to free up SQL connection pool
                }
            }
            catch (Exception ex)
            {
                FormatExceptionLog(ex, "AuditStudyRouting");
                //  Swallow exception and carry on
            }
            finally
            {
                //DebugLog($"\tBackground auditing task completed.");
            }
        }

        static void LogToTran_Audit(Study study, RemoteAE remoteAE, List<KeyValuePair<SendStatus, string>> realtedInstancesStatuses, MySqlConnection MySqlDb)
        {
            //  For POM server
            string formattedLog = GenerateAuditLog(study, remoteAE, realtedInstancesStatuses);

            if (MySqlDb?.State != ConnectionState.Open)
            {
                Log($"\t Skipping 'tran_audit' log insert because the MySQL DB connection is not ready!");
                return;
            }

            string sql = $"Select * from tran_audit where eid='{HandleQuotes(study.AccessionNumber)}' AND status_insert='DICOM AUTO ROUTE'";
            MySqlCommand cmd = new MySqlCommand(sql, MySqlDb);
            bool hasRows;
            using (MySqlDataReader reader = cmd.ExecuteReader())
            {
                hasRows = reader.HasRows;
                reader.Close();
            }
            if (hasRows)
            {
                DebugLog($"Updating existing entry in tran_audit table");

                sql = $"Update tran_audit set status = '{Utils.HandleQuotes(formattedLog)}', curr_date_time = '{DateTime.Now:yyyy-MM-dd hh:mm:ss}', curr_date = '{DateTime.Now:yyyy-MM-dd}'  where eid='{study.AccessionNumber}' AND status_insert='DICOM AUTO ROUTE'";
                cmd = new MySqlCommand(sql, MySqlDb);
                cmd.ExecuteNonQuery();
            }
            else
            {
                DebugLog($"Adding new entry into tran_audit table");

                sql = $"Insert into tran_audit (pid, eid, userid, status, status_insert, curr_date_time, curr_date) ";
                sql += $"VALUES ('{Utils.HandleQuotes(study.PatientID)}', '{Utils.HandleQuotes(study.AccessionNumber)}', '232', '{Utils.HandleQuotes(formattedLog)}', 'DICOM AUTO ROUTE', '{DateTime.Now:yyyy-MM-dd hh:mm:ss}', '{DateTime.Now:yyyy-MM-dd}')";
                cmd = new MySqlCommand(sql, MySqlDb);
                cmd.ExecuteNonQuery();
            }
            cmd.Dispose();
        }

        static string GenerateAuditLog(Study study, RemoteAE remoteAE, List<KeyValuePair<SendStatus, string>> ImageResults)
        {
            StringBuilder auditLog = new StringBuilder();

            int total = ImageResults.Count();
            int successfulImagesCount = ImageResults.Count(x => x.Key == SendStatus.Sent);
            var failedImages = ImageResults.Where(x => x.Key == SendStatus.Failed || x.Key == SendStatus.FailedMaxTimes);
            int failedImagesCount = failedImages.Count();
            string destinationName = string.IsNullOrEmpty(remoteAE.DestinationName) ? remoteAE.CalledAET : remoteAE.DestinationName;

            auditLog.Append($"Study <b>[{study.StudyDescription}]</b>, AccessionNumber <b>[{study.AccessionNumber}]</b> ");

            if (successfulImagesCount > 0)
            {
                auditLog.Append($"routed <b>{successfulImagesCount}/{total}</b> images");

                if (failedImagesCount > 0)
                    auditLog.Append($" and <b>{failedImagesCount}</b> {(failedImagesCount == 1 ? "Error" : "Errors")}");

            }
            else if (total == failedImagesCount)
                auditLog.Append($"has completely Failed when Routing");

            auditLog.Append($" to <b>[{destinationName}]</b>.");       //  End of log sentense


            //  Parse errors if any
            if (failedImagesCount > 0)
            {
                auditLog.Append($"<br>Errors when routing to <b>[{destinationName}]</b>:<br>");

                foreach (var groupedErrors in failedImages.GroupBy(x => x.Value))
                {
                    string placeholder = groupedErrors.Count() == 1 ? "Image" : "Images";
                    auditLog.Append($"<b>{groupedErrors.Count()}</b> {placeholder} failed to send with error '{groupedErrors.Key}'<br>");
                }
            }
            DebugLog($"Formatted Audit log:  {auditLog.ToString()}");
            return auditLog.ToString();
        }
    }
}
