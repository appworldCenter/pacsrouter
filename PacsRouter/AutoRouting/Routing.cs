﻿using DicomObjects;
using DicomObjects.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using static PACSRouter.Utils;


namespace PACSRouter
{
    class Routing
    {
        internal List<RemoteAE> FindRemoteAEForRouting(DicomDataSet Instance, PACSRouterDBContext db)
        {
            var result = new Dictionary<int, RemoteAE>();
            try
            {
                foreach (var rule in db.RoutingRules)
                {
                    Keyword ruleAttribute = DicomGlobal.Keyword((ushort)rule.Group, (ushort)rule.Element);
                    if (Instance[ruleAttribute].ExistsWithValue)
                    {
                        string val = string.Empty;
                        if (Instance[ruleAttribute].Value is string[])
                        {
                            string[] vals = Instance[ruleAttribute].Value as string[];
                            if (vals != null && vals.Length > 0)
                                val = vals[0].ToUpper();
                        }
                        else
                            val = Instance[ruleAttribute].Value.ToString().ToUpper();

                        if (val.Contains(rule.ExistingValue.ToUpper()))
                        {
                            RemoteAE remoteNode = rule.RemoteNode;

                            if (!remoteNode.Disable && !result.ContainsKey(remoteNode.ID))      // prevent duplicate endpoints
                                result.Add(remoteNode.ID, remoteNode);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log($"Error filtering RemoteAE {Environment.NewLine}{ex.Message}");
            }

            //DebugLog($"{result.Count} routing rules to apply");
            return result.Values.ToList();
        }

        internal void CreatingRoutingStatus(PACSRouterDBContext db, Study study, Series series, RemoteAE remoteAE)
        {
            try
            {
                if (!remoteAE.AlwaysForward)     //  Skip auditing for Local PACS
                {
                    //  Find or create Routing status, Check if  Study -> Series -> RemoteAE already exists
                    var routingStatus = db.RoutingStatuses.FirstOrDefault(x => x.Study.StudyUID == study.StudyUID && x.Series.SeriesUID == series.SeriesUID && x.RemoteNode.ID == remoteAE.ID);

                    if (routingStatus == null)
                    {
                        routingStatus = new RoutingStatus { Study = study, Series = series, RemoteNode = remoteAE };
                        db.RoutingStatuses.Add(routingStatus);
                        //DebugLog($"Adding new Routing status for StudyUID: {study.StudyUID}\tSeriesUID:{series.SeriesUID}\tRemoteAE: {remoteAE.ID}");
                    }
                    else if (routingStatus.Audited)
                    {
                        routingStatus.Audited = false;      //  Requeuing for Auditing
                    }
                }
            }
            catch (Exception ex)
            {
                Log($"Error CreatingRoutingStatus {Environment.NewLine}{ex.Message}");
            }
        }
    }
}
