﻿using DicomObjects;
using Org.BouncyCastle.Crypto.Tls;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace PACSRouter
{
    class MinimalWindowsClient
    {
        internal Stream TlsStream(Stream InnerStream, string RemoteAddress, int RemotePort)
        {
            var s = new SslStream(InnerStream, false, ValidateWindowsCertificate, GetClientCertificate);
            s.AuthenticateAsClient(RemoteAddress);
            return s;
        }

        private X509Certificate GetClientCertificate(object sender, string targetHost, X509CertificateCollection localCertificates, X509Certificate remoteCertificate, string[] acceptableIssuers)
        {
                return null;        //  No client-side certificate specifiedb!
        }

        //this one is for the SCU to validate the server's certificate
        public bool ValidateWindowsCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            // this is the Windows validator for the client to check the server's certificate
            // Most checks are disabled for testing purposes, leaving only absence of a client certificate if requested
            if (sslPolicyErrors.HasFlag(SslPolicyErrors.RemoteCertificateNotAvailable))
                return false;

            Utils.Log(certificate?.Subject + " (Windows)");
            return true;
        }
    }

    class MinimalBouncyCastleClient : DefaultTlsClient, TlsAuthentication
    {
        public Stream TlsStream(Stream OuterStream, string Address, int Port)
        {
            TlsClientProtocol handler = new TlsClientProtocol(OuterStream, new SecureRandom());
            handler.Connect(this);
            return handler.Stream;
        }

        public override TlsAuthentication GetAuthentication() { return this; }

        public TlsCredentials GetClientCredentials(CertificateRequest certificateRequest)
        {
                return null;        //  No client-side certificate specified!
        }

        public DefaultTlsSignerCredentials Credentials(TlsContext Context, byte[] PfxFile, string Password)
        {
            Pkcs12Store pfx = new Pkcs12Store(new MemoryStream(PfxFile), Password.ToCharArray());
            string alias = (from string a in pfx.Aliases where pfx.GetCertificateChain(a) != null select a).First();
            var ClientCertificate = new Certificate(Array.ConvertAll(pfx.GetCertificateChain(alias), (x) => x.Certificate.CertificateStructure));
            var ClientPrivateKey = pfx.GetKey(alias).Key;
            return new DefaultTlsSignerCredentials(Context, ClientCertificate, ClientPrivateKey);
        }

        public void NotifyServerCertificate(Certificate serverCertificate)
        {
            Utils.Log(serverCertificate.GetCertificateAt(0).Subject + " (BouncyCastle)");
        }
    }
}
