﻿namespace PACSRouter
{
    public enum SendStatus
    {
        Received = 0,
        Sent = 1,
        Failed = 2,
        FailedMaxTimes = 3,
    }

    public enum ConnectionType
    {
        None = 0,
        Windows = 1,
        BouncyCastle = 2
    }

    public enum StatusCodes
    {
        Success = 0,
        DuplicateInstanceReceived = 0x0111,

        RefusedOutofresources = 0xA700,
        MoveDestinationUnknown = 0xA801,

        MoveSubOperationCompleteWithOneORMoreFailures = 0xB000,

        GenericError = 0xC000,
        ReceivedInstanceFailedInternalValidation = 0xC001,
        CFindUnableToProcess = 0xC003,
        CMoveUnableToProcess = 0xC004,
        CGetUnableToProcess = 0xC005,
        InvalidQueryRoot = 0xC006,
        InvalidQueryLevel = 0xC007,

        Pending = 0xFF00,
    }
}