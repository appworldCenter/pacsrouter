﻿using DicomObjects;
using DicomObjects.Enums;
using DicomObjects.EventArguments;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Threading;
using static PACSRouter.Utils;

namespace PACSRouter
{
    partial class PACSRouter
    {
        void AssociationRequest(object Sender, AssociationRequestArgs e)
        {
            try
            {
                bool acceptContext = false;

                foreach (DicomContext context in e.Contexts)
                {
                    var tsMatch = PreferredTSs.Where(x => context.OfferedTS.Contains(x));
                    if (tsMatch.Any())
                    {
                        //  Make sure it's ordered by preference according to PreferredTSs table
                        tsMatch = tsMatch.OrderBy(x => x);
                        context.AcceptedTS = tsMatch.First();

                        //DebugLog($"Accepted TS:\t[{context.AcceptedTS}]");
                        acceptContext = true;
                    }
                    else
                    {
                        context.Reject(4); // transfer syntax not supported
                        Log($"Unsupported Transfer Syntax: [{string.Join(", ", context.OfferedTS)}]{Environment.NewLine}\tFor SOP class '{context.AbstractSyntax}'  on association ({context.ContextID})");
                    }
                }

                if (!acceptContext) //   if no DICOM context then reject the whole incoming association
                    e.Reject(1, 1, 2);  // 1-rejected permanent  3-service-provider-rejection(presentation related)  2- app-context-not-supported

                if (acceptContext)
                    e.Association.Tag = GetDB();                   //  Create new DB for this association
            }
            catch (Exception ex)
            {
                FormatExceptionLog(ex, "AssociationRequest");
                //  Swallow exception and carry on
            }
        }
        void InstanceReceived(object Sender, InstanceReceivedArgs e)
        {
            try
            {
                if (!ValidatePatient(e.Instance))
                {
                    Log($"Unknown Patient/Study rejected, pid={e.Instance.PatientID} an={e.Instance.AccessionNumber}");
                    e.Errors.Add(Keyword.ErrorComment, "Unknown patient/study rejected");
                    e.Status = (int)StatusCodes.GenericError;
                    return;
                }

                if (string.IsNullOrEmpty(e.Instance.StudyUID) ||
                    string.IsNullOrEmpty(e.Instance.SeriesUID) ||
                    string.IsNullOrEmpty(e.Instance.InstanceUID))
                {
                    Log($"UID cannot be null");
                    e.Errors.Add(Keyword.ErrorComment, "Study/Series/Instance UID cannot be null");
                    e.Status = (int)StatusCodes.GenericError;
                    return;
                }

                //  Creating DB connection if needed
                var db = (PACSRouterDBContext)e.RequestAssociation.Tag;
                if (db == null)
                {
                    db = GetDB();                   //  Create new DB
                    e.RequestAssociation.Tag = db;
                }

                var instanceReceivedTimeStamp = DateTime.Now;

                //   Digest received instance by building DB entries to form the structure
                Study study = Study.CreateIfNecessary(e.Instance, instanceReceivedTimeStamp, db);
                Series series = Series.CreateIfNecessary(e.Instance, study, instanceReceivedTimeStamp, db);

                //  Can be more than 1 entry if there are multiple Remote AEs but share the same Filename
                string filename;
                if (!S3StorageService.Store(e.Instance, out filename))
                {
                    // failed to store the received images
                    e.Errors.Add(Keyword.ErrorComment, "Failed to store received images");
                    e.Status = (int)StatusCodes.GenericError;
                    return;
                }

                //  Routing instance based on matching rules
                var remoteAEsToRoute = InstanceRouting.FindRemoteAEForRouting(e.Instance, db);

                //  Check for overriding flag, applicable for main PACS                    
                remoteAEsToRoute.AddRange(db.RemoteAEs.Where(x => x.AlwaysForward && !x.Disable));

                bool dbUpdateRequired = false;
                foreach (var remoteAE in remoteAEsToRoute)
                {
                    //  Checking if duplicate instances received for this RemoteAE
                    if (db.Instances.Any(x => x.InstanceUID == e.Instance.InstanceUID && remoteAE.ID == x.RemoteAEID))
                    {
                        Log($"Duplicate instance '{e.Instance.InstanceUID}' received for [{remoteAE.CalledAET}], discard and continue");
                        continue;
                    }

                    Instance instance = new Instance(e.Instance, series, remoteAE, filename, instanceReceivedTimeStamp);
                    db.Instances.Add(instance);

                    //  Start routing status for received Instance
                    InstanceRouting.CreatingRoutingStatus(db, study, series, remoteAE);

                    //  Directly send Images to Local PACS
                    if (IsDestinationLocalPACS(remoteAE))
                        ForwardToLocalPACS(remoteAE, instance);

                    dbUpdateRequired = true;
                }

                if (dbUpdateRequired)
                {
                retry:
                    try
                    {
                        //  On successful write to disk, save DB changes
                        db.SaveChanges();
                    }
                    catch (EntityException ex)
                    {
                        FormatExceptionLog(ex, "DB SaveChanges");
                        Thread.Sleep(500);
                        goto retry;
                    }
                }

                int currentThreadID = Thread.CurrentThread.ManagedThreadId;
                //  Then remove related UIDs from tracking so the looping thread resumes
                if (newStudyUIDs.Contains(new KeyValuePair<string, int>(study.StudyUID, currentThreadID)))
                    newStudyUIDs.TryRemove(study.StudyUID, out int v);
                if (newSeriesUIDs.Contains(new KeyValuePair<string, int>(series.SeriesUID, currentThreadID)))
                    newSeriesUIDs.TryRemove(series.SeriesUID, out int v);

                //  Always return Success
                e.Status = (int)StatusCodes.Success;
            }
            catch (Exception ex)
            {
                FormatExceptionLog(ex, "InstanceReceived");
                //  Swallow exception and return error status

                e.Errors.Add(Keyword.ErrorComment, ex.Message);
                e.Status = (int)StatusCodes.GenericError;
            }
        }


        void AssociationClosed(object Sender, AssociationClosedArgs e)
        {
            try
            {
                if (e.Association.Tag != null)
                {
                    var db = (PACSRouterDBContext)e.Association.Tag;

                    if (db.Database.Connection.State == System.Data.ConnectionState.Open)
                        db.Database.Connection.Close();     //  Explicitly closing to try to free up SQL connection pool

                    e.Association.Tag = null;
                    //DebugLog($"  Closing DB connection for association #{e.Association.AssociationNumber}");
                }
            }
            catch { }
        }
    }
}
