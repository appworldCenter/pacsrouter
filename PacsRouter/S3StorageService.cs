﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using DicomObjects;
using DicomObjects.Enums;
using System;
using System.IO;

namespace PACSRouter
{
    public static class S3StorageService
    {
        public static string BucketName { get; set; }
        public static string ImageFoldername { get; set; }
        private const string accessKeyID = "AKIA32BK6623PO47A7MV";
        private const string secretAccessKey = "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";

        public static bool Store(DicomDataSet Instance, out string partialFilename)
        {
            try
            {
                AmazonS3Client client = new AmazonS3Client(accessKeyID, secretAccessKey);
                TransferUtility utility = new TransferUtility(client);
                string subDir;
                string filename;
                GetPath(Instance, out subDir, out filename);

                TransferUtilityUploadRequest req = new TransferUtilityUploadRequest()
                {
                    BucketName = $"{BucketName}/{ImageFoldername}/{subDir}",
                    Key = filename,
                };

#pragma warning disable CS0618 // Type or member is obsolete
                Instance.ValidationTypes = ValidationTypes.None;
#pragma warning restore CS0618 // Type or member is obsolete
                MemoryStream ms = new MemoryStream();
                Instance.Write(ms, Instance.OriginalTS);
                ms.Position = 0;
                req.InputStream = ms;
                utility.Upload(req);
                partialFilename = $"{subDir}/{filename}";
                return true;
            }
            catch (Exception ex)
            {
                DicomGlobal.Log($"Error Storing Image - {ex.Message}");
                DicomGlobal.Log(ex.StackTrace);
                partialFilename = "";
                return false;
            }
        }

        private static void GetPath(DicomDataSet ds, out string subDir, out string filename)
        {
            string modality, studyDate, patientID;
            modality = ds[Keyword.Modality].ExistsWithValue ? ds[Keyword.Modality].Value.ToString() : "NULL_Modality";
            studyDate = ds[Keyword.StudyDate].ExistsWithValue ? Convert.ToDateTime(ds[Keyword.StudyDate].Value.ToString()).ToString("MM-dd-yyyy") : "00-00-00";
            patientID = ds[Keyword.PatientID].ExistsWithValue ? ds[Keyword.PatientID].Value.ToString() : "NULL_PID";

            subDir = $"{modality}/{studyDate}/{patientID}".Replace("*", "");
            foreach (char c in Path.GetInvalidPathChars())
                subDir = subDir.Replace(c, '_');

            filename = Guid.NewGuid().ToString() + ".dcm";
        }

        public static void GetS3Location(string partialFilename, out string subDir, out string key)
        {
            key = partialFilename.Substring(partialFilename.LastIndexOf("/") + 1);
            subDir = partialFilename.Substring(0, partialFilename.LastIndexOf("/"));
        }

        public static DicomDataSet GetInstance(string partialFilename, bool needPixel)
        {
            try
            {
                if (partialFilename.ToUpper().StartsWith(@"P:\IMAGES\")) // absolute path
                {
                    DicomDataSet ds = new DicomDataSet(partialFilename);
                    if (!needPixel)
                    {
                        ds.Add(0x7019, 0x1010, "LT", partialFilename); // tell PACS where to find it
                        ds.Remove(Keyword.PixelData);
                    }
                    return ds;
                }
                else
                {
                    AmazonS3Client client = new AmazonS3Client(accessKeyID, secretAccessKey);
                    string bucketName;
                    string key;
                    GetS3Location(partialFilename, out bucketName, out key);

                    GetObjectRequest request = new GetObjectRequest
                    {
                        BucketName = $"{BucketName}/{ImageFoldername}/{bucketName}",
                        Key = key
                    };

                    using (GetObjectResponse response = client.GetObject(request))
                    using (Stream responseStream = response.ResponseStream)
                    {
                        DicomDataSet ds = new DicomDataSet(responseStream);

                        if (!needPixel)
                        {
                            ds.Add(0x7019, 0x1010, "LT", partialFilename); // tell PACS where to find it
                            ds.Remove(Keyword.PixelData); // no need to send pixel data
                        }
                        return ds;
                    }
                }
            }
            catch (Exception ex)
            {
                DicomGlobal.Log($"Error loading Image from {BucketName}/{ImageFoldername}/{partialFilename} : {ex.Message}");
                DicomGlobal.Log(ex.StackTrace);
                return null;
            }
        }
    }
}
