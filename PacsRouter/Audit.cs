﻿using DicomObjects;
using DicomObjects.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using static PACSRouter.PACSRouter;

namespace PACSRouter
{
    public class AuditLogger
    {
        #region Internal variables
        Timer timer;
        #endregion

        internal void Start()
        {
            //  How frequently do we run the Sender routine
            int auditFrequency = InitConfig.AuditInterval * 60 * 1000;   // convert to milliseconds
            timer = new Timer(AuditRoutine, null, 0, auditFrequency);    //  Resume tasks on Interval elapsed
        }
        private void AuditRoutine(object o)
        {
            try
            {
                using (var db = GetDB())
                {
                    //  Select logs that were first logged less than a day ago 
                    //  or that have never been updated to do an Update
                    DateTime dayAgo = DateTime.Now.AddDays(-1);

                    var auditsWithinAday = db.Audits.Where(x => dayAgo < x.InsertTimestamp || x.UpdateTimestamp == null);

                    foreach (var audit in auditsWithinAday)
                        UpdateAudit(audit, db);

                    //  Select logs that were first logged less than 7 days ago
                    //  and do less frequent update based on UpdateTimestamp
                    DateTime weekAgo = DateTime.Now.AddDays(-7);
                    DateTime hourAgo = DateTime.Now.AddMinutes(-60);

                    var auditsWithin10days = db.Audits.Where(x => weekAgo < x.InsertTimestamp && (x.UpdateTimestamp == null || hourAgo > x.UpdateTimestamp)).Except(auditsWithinAday);

                    DebugLog(auditsWithin10days.Count().ToString());

                    foreach (var audit in auditsWithin10days)
                        UpdateAudit(audit, db);

                    if (auditsWithinAday.Any() || auditsWithin10days.Any())
                        db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                FormatExceptionLog(ex, "AuditRoutine");
                //  Swallow exception and carry on
            }
        }
    }
}
