﻿namespace PACSRouter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Configurations",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ImplementationName = c.String(maxLength: 16),
                        ImplementationUID = c.String(maxLength: 64),
                        CacheDays = c.Int(nullable: false),
                        StorageRootDirectory = c.String(nullable: false),
                        MaxNumberOfRetries = c.Int(nullable: false),
                        SenderTaskFrequency = c.Int(nullable: false),
                        MaintenanceTaskFrequency = c.Int(nullable: false),
                        MaxNumberOfSenderThreads = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Instances",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RemoteAEID = c.Int(nullable: false),
                        StorageTS = c.String(nullable: false, maxLength: 64),
                        InstanceUID = c.String(nullable: false, maxLength: 64),
                        SOPClassUID = c.String(maxLength: 64),
                        DateReceived = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        FileLocation = c.String(nullable: false, maxLength: 500),
                        LastResult = c.Int(),
                        LastAttempt = c.DateTime(precision: 7, storeType: "datetime2"),
                        NumberOfFailedAttempts = c.Int(nullable: false),
                        SendStatus = c.Int(nullable: false),
                        Series_SeriesUID = c.String(maxLength: 64),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Series", t => t.Series_SeriesUID)
                .Index(t => t.Series_SeriesUID);
            
            CreateTable(
                "dbo.Series",
                c => new
                    {
                        SeriesUID = c.String(nullable: false, maxLength: 64),
                        SeriesDescription = c.String(maxLength: 64),
                        Modality = c.String(nullable: false, maxLength: 10),
                        SeriesDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        SeriesTime = c.DateTime(precision: 7, storeType: "datetime2"),
                        PerformingPhysicianName = c.String(maxLength: 200),
                        RequestingPhysician = c.String(maxLength: 200),
                        ScheduledPerformingPhysicianName = c.String(maxLength: 200),
                        SendStatus = c.Int(nullable: false),
                        Study_StudyUID = c.String(maxLength: 64),
                    })
                .PrimaryKey(t => t.SeriesUID)
                .ForeignKey("dbo.Studies", t => t.Study_StudyUID)
                .Index(t => t.Study_StudyUID);
            
            CreateTable(
                "dbo.Studies",
                c => new
                    {
                        StudyUID = c.String(nullable: false, maxLength: 64),
                        AccessionNumber = c.String(maxLength: 16),
                        StudyID = c.String(maxLength: 16),
                        ReferringPhysicianName = c.String(maxLength: 64),
                        StudyDescription = c.String(maxLength: 64),
                        StudyDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        PatientID = c.String(maxLength: 64),
                        PatientName = c.String(nullable: false, maxLength: 250),
                        PatientDateOfBirth = c.DateTime(precision: 7, storeType: "datetime2"),
                        PatientSex = c.String(maxLength: 16),
                        SendStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.StudyUID);
            
            CreateTable(
                "dbo.ListeningPorts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PortNumber = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PreferredTS",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TransferSyntax = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.RemoteAEs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        IP = c.String(nullable: false),
                        Port = c.Int(nullable: false),
                        CallingAET = c.String(nullable: false),
                        CalledAET = c.String(nullable: false),
                        Encryption = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Instances", "Series_SeriesUID", "dbo.Series");
            DropForeignKey("dbo.Series", "Study_StudyUID", "dbo.Studies");
            DropIndex("dbo.Series", new[] { "Study_StudyUID" });
            DropIndex("dbo.Instances", new[] { "Series_SeriesUID" });
            DropTable("dbo.RemoteAEs");
            DropTable("dbo.PreferredTS");
            DropTable("dbo.ListeningPorts");
            DropTable("dbo.Studies");
            DropTable("dbo.Series");
            DropTable("dbo.Instances");
            DropTable("dbo.Configurations");
        }
    }
}
