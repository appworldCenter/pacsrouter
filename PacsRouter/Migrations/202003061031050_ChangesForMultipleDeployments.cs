﻿namespace PACSRouter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesForMultipleDeployments : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Audits", "Series_SeriesUID", "dbo.Series");
            DropForeignKey("dbo.Audits", "Study_StudyUID", "dbo.Studies");
            DropIndex("dbo.Audits", new[] { "Series_SeriesUID" });
            DropIndex("dbo.Audits", new[] { "Study_StudyUID" });
            AddColumn("dbo.Series", "DateReceived", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Studies", "DateReceived", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Instances", "ErrorComment", c => c.String());
            DropTable("dbo.Audits");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Audits",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuditMessage = c.String(nullable: false),
                        InstancesCount = c.Int(),
                        InsertTimestamp = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdateTimestamp = c.DateTime(precision: 7, storeType: "datetime2"),
                        Series_SeriesUID = c.String(maxLength: 64),
                        Study_StudyUID = c.String(maxLength: 64),
                    })
                .PrimaryKey(t => t.ID);
            
            DropColumn("dbo.Instances", "ErrorComment");
            DropColumn("dbo.Studies", "DateReceived");
            DropColumn("dbo.Series", "DateReceived");
            CreateIndex("dbo.Audits", "Study_StudyUID");
            CreateIndex("dbo.Audits", "Series_SeriesUID");
            AddForeignKey("dbo.Audits", "Study_StudyUID", "dbo.Studies", "StudyUID");
            AddForeignKey("dbo.Audits", "Series_SeriesUID", "dbo.Series", "SeriesUID");
        }
    }
}
