﻿namespace PACSRouter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MovedStudyRoutingFromPacs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RoutingRules",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ExistingValue = c.String(nullable: false),
                        Group = c.Short(nullable: false),
                        Element = c.Short(nullable: false),
                        Comments = c.String(),
                        RemoteNode_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.RemoteAEs", t => t.RemoteNode_ID)
                .Index(t => t.RemoteNode_ID);
            
            CreateTable(
                "dbo.RoutingStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Audited = c.Boolean(nullable: false),
                        SendStatus = c.Int(nullable: false),
                        RemoteNode_ID = c.Int(),
                        Series_SeriesUID = c.String(maxLength: 64),
                        Study_StudyUID = c.String(maxLength: 64),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.RemoteAEs", t => t.RemoteNode_ID)
                .ForeignKey("dbo.Series", t => t.Series_SeriesUID)
                .ForeignKey("dbo.Studies", t => t.Study_StudyUID)
                .Index(t => t.RemoteNode_ID)
                .Index(t => t.Series_SeriesUID)
                .Index(t => t.Study_StudyUID);
            
            AddColumn("dbo.RemoteAEs", "AlwaysForward", c => c.Boolean(nullable: false));
            AddColumn("dbo.RemoteAEs", "Disable", c => c.Boolean(nullable: false));
            AddColumn("dbo.RemoteAEs", "DestinationName", c => c.String(maxLength: 64));
            DropColumn("dbo.Configurations", "AuditInterval");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Configurations", "AuditInterval", c => c.Int(nullable: false));
            DropForeignKey("dbo.RoutingStatus", "Study_StudyUID", "dbo.Studies");
            DropForeignKey("dbo.RoutingStatus", "Series_SeriesUID", "dbo.Series");
            DropForeignKey("dbo.RoutingStatus", "RemoteNode_ID", "dbo.RemoteAEs");
            DropForeignKey("dbo.RoutingRules", "RemoteNode_ID", "dbo.RemoteAEs");
            DropIndex("dbo.RoutingStatus", new[] { "Study_StudyUID" });
            DropIndex("dbo.RoutingStatus", new[] { "Series_SeriesUID" });
            DropIndex("dbo.RoutingStatus", new[] { "RemoteNode_ID" });
            DropIndex("dbo.RoutingRules", new[] { "RemoteNode_ID" });
            DropColumn("dbo.RemoteAEs", "DestinationName");
            DropColumn("dbo.RemoteAEs", "Disable");
            DropColumn("dbo.RemoteAEs", "AlwaysForward");
            DropTable("dbo.RoutingStatus");
            DropTable("dbo.RoutingRules");
        }
    }
}
