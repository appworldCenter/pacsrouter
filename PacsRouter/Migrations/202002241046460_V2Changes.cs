﻿namespace PACSRouter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class V2Changes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Audits",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuditMessage = c.String(nullable: false),
                        InstancesCount = c.Int(),
                        InsertTimestamp = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdateTimestamp = c.DateTime(precision: 7, storeType: "datetime2"),
                        Series_SeriesUID = c.String(maxLength: 64),
                        Study_StudyUID = c.String(maxLength: 64),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Series", t => t.Series_SeriesUID)
                .ForeignKey("dbo.Studies", t => t.Study_StudyUID)
                .Index(t => t.Series_SeriesUID)
                .Index(t => t.Study_StudyUID);
            
            AddColumn("dbo.Configurations", "AuditInterval", c => c.Int(nullable: false));
            AddColumn("dbo.Instances", "SendPriority", c => c.Long(nullable: false));
            AddColumn("dbo.Studies", "PatientFirstName", c => c.String(maxLength: 50));
            AddColumn("dbo.Studies", "PatientLastName", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Audits", "Study_StudyUID", "dbo.Studies");
            DropForeignKey("dbo.Audits", "Series_SeriesUID", "dbo.Series");
            DropIndex("dbo.Audits", new[] { "Study_StudyUID" });
            DropIndex("dbo.Audits", new[] { "Series_SeriesUID" });
            DropColumn("dbo.Studies", "PatientLastName");
            DropColumn("dbo.Studies", "PatientFirstName");
            DropColumn("dbo.Instances", "SendPriority");
            DropColumn("dbo.Configurations", "AuditInterval");
            DropTable("dbo.Audits");
        }
    }
}
