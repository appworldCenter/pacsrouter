﻿using DicomObjects;
using DicomObjects.Delegates;
using DicomObjects.EventArguments;
using PACSRouter.Properties;
using System;
using System.ServiceProcess;

namespace PACSRouter
{
    public partial class PACSRouterService : ServiceBase
    {
        PACSRouter router;
        public PACSRouterService()
        {
            InitializeComponent();
        }
        protected override void OnStart(string[] args)
        {
            router = new PACSRouter();
            router.Start();
        }

        protected override void OnStop()
        {
            router.Stop();
        }
    }
}
