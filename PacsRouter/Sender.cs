﻿using DicomObjects;
using DicomObjects.Delegates;
using DicomObjects.DicomUIDs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using static PACSRouter.Audit;
using static PACSRouter.PACSRouter;
using static PACSRouter.Utils;

namespace PACSRouter
{
    internal class Sender
    {
        #region Internal variables     
        Timer timer;
        volatile bool running = false;

        private static int batchCount
        {
            get { return Properties.Settings.Default.NoOfImagesPerBatch; }
        }

        #endregion
        internal void Start()
        {
            DicomGlobal.Timeout = 300000;   //  5 minutes
            int senderFrequency = InitConfig.SenderTaskFrequency * 60 * 1000;   // convert to milliseconds

            //  How frequently do we run the Sender routine
            timer = new Timer(SenderRoutine, null, 0, senderFrequency);      //  Resume tasks on Interval elapsed
            Log($"Sender thread started at {DateTime.Now}");
        }

        internal void SenderRoutine(object o)
        {
            if (running) return;

            try
            {
                running = true;

                List<IGrouping<Study, Instance>> instances_StudyGrouped = new List<IGrouping<Study, Instance>>();
                RemoteAE[] remoteAEs = new RemoteAE[0];
                using (var db = GetDB())
                {
                    //  Allowing a period of time for all images for a Study to be received before sending the batch
                    DateTime studyReceivingWindowTimeout = DateTime.Now.AddMinutes(Properties.Settings.Default.StudyReceivingTimeout * -1);
                    DateTime retryTimeout = DateTime.Now.AddMinutes(-10);

                    DateTime startingValid = DateTime.UtcNow.AddDays(InitConfig.CacheDays * -1);

                    //  Check DB instances table to find instances to send
                    instances_StudyGrouped = GetInstancesToSend(db, studyReceivingWindowTimeout, x => (x.SendStatus == SendStatus.Received || x.SendStatus == SendStatus.Failed) && x.DateReceived >= startingValid);

                    //  Refresh RemoteAE list
                    remoteAEs = db.RemoteAEs.ToArray();

                    db.Database.Connection.Close();     //  Explicitly closing to try to free up SQL connection pool
                }
                if (instances_StudyGrouped.Any())
                {
                    var sendLoopResult = Parallel.ForEach(instances_StudyGrouped,
                        new ParallelOptions { MaxDegreeOfParallelism = 2 },
                        instancesPerStudy =>
                    {
                        //  Create background tasks for each Destination
                        var instances_RemoteAEGrouped = instancesPerStudy.GroupBy(x => x.RemoteAEID);

                        //  Select Local PACS first to give send priority
                        var instances_LocalPACS = instances_RemoteAEGrouped.Where(x => x.Key == remoteAEs.First(y => IsDestinationLocalPACS(y)).ID);

                        //  List other PACS
                        var instances_OtherRemoteAEs = instances_RemoteAEGrouped.Where(x => !instances_LocalPACS.Any(y => y.Key == x.Key));


                        //  Send to Local PACs the images the could have failed to send in Receiver.ForwardToLocalPACS
                        foreach (var instancesPerAE in instances_LocalPACS)
                            SendBatchedInstances(instancesPerAE, remoteAEs);

                        //  Then Send to rest of the RemoteAEs
                        Parallel.ForEach(instances_OtherRemoteAEs,
                            new ParallelOptions { MaxDegreeOfParallelism = 1 },
                            instancesPerAE => SendBatchedInstances(instancesPerAE, remoteAEs));
                    });

                    if (sendLoopResult.IsCompleted)
                    {
                        //   After all sending has completed
                        CheckandUpdateSendStatuses();

                        //  Generate audits in Background
                        ThreadPool.QueueUserWorkItem(new WaitCallback(AuditStudyRouting));
                    }
                    else
                        DebugLog($"\tSenderRoutine failed to send all");
                }
            }
            catch (Exception ex)
            {
                FormatExceptionLog(ex, "Sender");
                //  Swallow exception and carry on
            }
            finally
            {
                running = false;
                //GC.Collect();                    //  Force clean up of unused memory
                //DebugLog($"Sender thread sleeping... Zzz");
            }
        }

        #region Sender helper routines
        private void SendBatchedInstances(IGrouping<int, Instance> instancesPerAE, RemoteAE[] RemoteAEs)
        {
            try
            {
                RemoteAE remoteAE = RemoteAEs.SingleOrDefault(x => x.ID == instancesPerAE.Key);

                if (remoteAE != null)
                {
                    //DebugLog($"\tQueuing ({instancesPerAE.Count()}) instances to send to {remoteAE.CalledAET} @ {remoteAE.IP}:{remoteAE.Port}");

                    DICOMSend(instancesPerAE, remoteAE, true);
                }
                else
                    Log($"RemoteAE ID #{instancesPerAE.Key} not found");
            }
            catch (Exception ex)
            {
                FormatExceptionLog(ex, $"Sending ({instancesPerAE.Count()}) instances to RemoteAE ID: {instancesPerAE.Key}");
                //  Swallow exception and carry on
            }
        }

        private List<IGrouping<Study, Instance>> GetInstancesToSend(PACSRouterDBContext db, DateTime studyReceivingWindowTimeout, Expression<Func<Instance, bool>> predicate)
        {
            List<IGrouping<Study, Instance>> instances_StudyGrouped = new List<IGrouping<Study, Instance>>();

            var allInstancesToSend = db.Instances.Where(predicate);

            foreach (var instancesStudyGroup in allInstancesToSend.GroupBy(x => x.Series.Study))
            {
                if (instancesStudyGroup.OrderBy(x => x.DateReceived).Last().DateReceived < studyReceivingWindowTimeout)     //  Consider all images in the study received after timeout period
                    instances_StudyGrouped.Add(instancesStudyGroup);
            }
            return instances_StudyGrouped;
        }

        internal static void DICOMSend(IEnumerable<Instance> InstancesToSend, RemoteAE remoteAE, bool RefreshContextAndSave)
        {
            //  Sort based on send priority
            var sortedInstances = InstancesToSend.OrderByDescending(x => x.SendPriority);

            //  Queue for worker threads
            Queue sendQueue = Queue.Synchronized(new Queue(CreateBatches(sortedInstances, batchCount)));

            //  Create worker threads to poll queue for data
            Thread[] workerThreads = new Thread[Math.Min(InitConfig.MaxNumberOfSenderThreads, sendQueue.Count)];

            sendQueue.Enqueue(null);        //  To mark end of current Queue

            for (int i = 0; i < workerThreads.Length; i++)
            {
                workerThreads[i] = new Thread(new ThreadStart(() =>
                {
                    try
                    {
                        while (sendQueue.Peek() != null)    //  Check for images to send in the queue
                        {
                            //  Get batch to process
                            IEnumerable<Instance> batchInstances = (IEnumerable<Instance>)sendQueue.Dequeue();

                            //Send instances to PACS                            
                            using (DicomAssociation assoc = new DicomAssociation())
                            {
                                bool dbUpdateRequired = false;
                                try
                                {
                                    TlsInitiator GetTLSStream = null;       //  Default is unsecured

                                    //  Setup secured sending if needed
                                    switch (remoteAE.Encryption)
                                    {
                                        case ConnectionType.Windows:
                                            DebugLog($"Using WindowsClient authentication");
                                            MinimalWindowsClient WindowsClient = new MinimalWindowsClient();
                                            GetTLSStream = WindowsClient.TlsStream;
                                            break;
                                        case ConnectionType.BouncyCastle:
                                            DebugLog($"Using BouncyCastleClient authentication");
                                            MinimalBouncyCastleClient BCClient = new MinimalBouncyCastleClient();
                                            GetTLSStream = BCClient.TlsStream;
                                            break;
                                    }

                                    //  Adding C-ECHO
                                    assoc.RequestedContexts.Add(SOPClasses.Verification, TransferSyntaxes.ExplicitVRLittleEndian);

                                    var sopClassGrouped = batchInstances.GroupBy(x => new { x.SOPClassUID, x.StorageTS });
                                    foreach (var g in sopClassGrouped)
                                    {
                                        //  Explicitly adding SOP Class and TS list to support non-default
                                        assoc.RequestedContexts.Add(g.Key.SOPClassUID, TStoPropose(g.Key.StorageTS, remoteAE.AlwaysForward));
                                    }
                                    if (IsPACSReachable(assoc, remoteAE, GetTLSStream))
                                    {
                                        dbUpdateRequired = true;

                                        foreach (var instance in batchInstances)
                                        {
                                            int status = 0;
                                            string errorComment = string.Empty;
                                            try
                                            {
                                                //  No need to open connection here, happens in DICOM echo
                                                if (!assoc.isOpen)
                                                {
                                                    DebugLog($"PACS [{remoteAE.CalledAET}] unreachable, most likely association is aborted!");
                                                    break;
                                                }

                                                //  Use original TS by choosing a Matching PCID
                                                var matchingContext = assoc.AgreedContexts.FirstOrDefault(x => x.AbstractSyntax == instance.SOPClassUID && x.AcceptedTS == instance.StorageTS)?.ContextID;
                                                if (matchingContext != null)
                                                    assoc.PreferredPCID = matchingContext.Value;
                                                else
                                                    assoc.PreferredPCID = 0;    //  Default

                                                //  Strip out pixel data for Local PACS send
                                                bool noNeedForPixel = IsDestinationLocalPACS(remoteAE);
                                                using (var ds = S3StorageService.GetInstance(instance.FileLocation, !noNeedForPixel))
                                                {
                                                    Debug.Assert(ds != null, "Missing file!");
                                                    assoc.SendInstances(ds);
                                                }
                                                status = assoc.LastStatus;
                                                errorComment = assoc.LastError;
                                            }
                                            catch (FileNotFoundException ex)
                                            {
                                                status = 3;
                                                errorComment = ex.Message;
                                                Log($" File not found: {ex.Message}");
                                            }
                                            catch (Exception ex)
                                            {
                                                status = (int)StatusCodes.GenericError;
                                                errorComment = ex.Message;
                                            }

                                            UpdateInstanceStatus(instance, status, errorComment);
                                        }
                                    }
                                    else
                                    {
                                        DebugLog($"\t\tPACS '{remoteAE.CalledAET}' unreachable");

                                        //  Save deferred sending status for unreachable destination
                                        batchInstances.ToList().ForEach(x =>
                                        {
                                            x.SendStatus = SendStatus.FailedMaxTimes;
                                            x.LastAttempt = DateTime.Now;
                                            x.ErrorComment = $"Destination '{remoteAE.DestinationName}' unreachable";
                                        });

                                        dbUpdateRequired = true;
                                        break;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    FormatExceptionLog(ex, Thread.CurrentThread.Name);
                                    //  Swallow exception and carry on
                                }
                                finally
                                {
                                    assoc.Close();

                                    if (RefreshContextAndSave && dbUpdateRequired)
                                    {
                                        using (var db = GetDB())
                                        {
                                            DebugLog($"{Thread.CurrentThread.Name} \tUpdating {batchInstances.Count()} instances.");
                                            foreach (var toUpdate in batchInstances)
                                            {
                                                //  Update instances by copying over relevant state
                                                var fromContext = db.Instances.First(x => x.ID == toUpdate.ID);
                                                fromContext.SendStatus = toUpdate.SendStatus;
                                                fromContext.LastAttempt = toUpdate.LastAttempt;
                                                fromContext.LastResult = toUpdate.LastResult;
                                                fromContext.NumberOfFailedAttempts = toUpdate.NumberOfFailedAttempts;
                                                fromContext.SendPriority = toUpdate.SendPriority;
                                                fromContext.ErrorComment = toUpdate.ErrorComment;
                                            }

                                            db.SaveChanges();
                                            db.Database.Connection.Close();     //  Explicitly closing to try to free up SQL connection pool
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (InvalidOperationException ex)
                    {
                        DebugLog($"{Thread.CurrentThread.Name}\t{ex.Message}");
                        //  Swallow queue peek exception and carry on
                    }
                }))
                {
                    IsBackground = true,
                    Name = $"SenderThread #{i}",
                    Priority = ThreadPriority.AboveNormal
                };
                workerThreads[i].Start();
            }

            //  Await all background threads
            foreach (var workerThread in workerThreads)
                workerThread.Join(600000);      //  10 minutes thread timeout
            //DebugLog($"---  All sender threads joined  ---");
        }
        #endregion
    }
}