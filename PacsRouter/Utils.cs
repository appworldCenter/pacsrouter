﻿using DicomObjects;
using DicomObjects.Delegates;
using DicomObjects.DicomUIDs;
using DicomObjects.Enums;
using MySql.Data.MySqlClient;
using PACSRouter.Properties;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Diagnostics;
using System.IO;
using System.Linq;
using static PACSRouter.PACSRouter;
using static PACSRouter.Sender;

namespace PACSRouter
{
    internal class Utils
    {
        internal static Settings ProjectSettings = Properties.Settings.Default;
        internal static bool IsDestinationLocalPACS(RemoteAE remoteAE)
        {
            return (remoteAE.IP.Equals("127.0.0.1") || remoteAE.IP.Equals("localhost")) && remoteAE.AlwaysForward;
        }

        //  For tracking new UIDs to avoid duplicates when received at parallely
        internal static ConcurrentDictionary<string, int> newStudyUIDs = new ConcurrentDictionary<string, int>();
        internal static ConcurrentDictionary<string, int> newSeriesUIDs = new ConcurrentDictionary<string, int>();

        #region Sender helpers
        internal static void UpdateInstanceStatus(Instance instanceToUpdate, int SendStatusCode, string ErrorComment)
        {
            try
            {
                //  Update send status for instance
                if (SendStatusCode == (int)StatusCodes.Success)
                {
                    instanceToUpdate.SendStatus = SendStatus.Sent;
                }
                else if (instanceToUpdate.NumberOfFailedAttempts + 1 < InitConfig.MaxNumberOfRetries)
                {
                    instanceToUpdate.SendStatus = SendStatus.Failed;
                    instanceToUpdate.NumberOfFailedAttempts++;        //  Stops at max allowed
                    instanceToUpdate.SendPriority--;        //  Decrease priority
                }
                else
                {
                    instanceToUpdate.SendStatus = SendStatus.FailedMaxTimes;
                    instanceToUpdate.SendPriority = 0;        //  Lowest priority
                }

                instanceToUpdate.LastAttempt = DateTime.Now;
                instanceToUpdate.LastResult = SendStatusCode;
                instanceToUpdate.ErrorComment = ErrorComment;
            }
            catch (Exception ex)
            {
                FormatExceptionLog(ex, "UpdateInstanceStatus");
                //  Swallow exception and carry on
            }
        }

        internal static MySqlConnection GetMySQLDB()
        {
            //DebugLog($"Opening MySQL connection to [{Settings.Default.MYSQL_ConnectionString}]");
            if (string.IsNullOrWhiteSpace(Settings.Default.MYSQL_ConnectionString))
                return null;

            var DBConnection = new MySqlConnection(Settings.Default.MYSQL_ConnectionString);
            DBConnection.Open();                //  Throw exception if failed to open
            return DBConnection;
        }

        internal static bool ValidatePatient(DicomDataSet ds)
        {
#if DEBUG
            if (ds.Name == "SendToTestPACS")
                return true;    //  Skip validation for test data
#endif
            using (var cn = GetMySQLDB())
            {
                MySqlCommand cmd = new MySqlCommand($"select id FROM tran_patient_detail WHERE patient_id='{HandleQuotes(ds.PatientID)}'", cn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    return reader.HasRows;
                }
            }
        }

        internal static void CheckandUpdateSendStatuses()
        {
            try
            {
                using (var db = GetDB())
                {
                    //  Take top down approach starting from Study table
                    bool dbUpdateRequired = false;
                    var allStudies = db.Studies.Where(x => x.SendStatus != SendStatus.Sent).ToArray();
                    for (int i = 0; i < allStudies.Count(); i++)
                    {
                        var study = allStudies[i];
                        var allSeries = db.Series.Where(x => x.Study.StudyUID == study.StudyUID && x.SendStatus != SendStatus.Sent).ToArray();
                        for (int j = 0; j < allSeries.Count(); j++)
                        {
                            var series = allSeries[j];
                            var allInstances = db.Instances.Where(x => x.Series.SeriesUID == series.SeriesUID);
                            if (allInstances.All(x => x.SendStatus == SendStatus.Sent) && series.SendStatus != SendStatus.Sent)
                            {
                                //  Update Series if all related instances are Sent and status is not already set
                                var fromContext = db.Series.Single(x => x.SeriesUID == series.SeriesUID);
                                fromContext.SendStatus = SendStatus.Sent;
                                dbUpdateRequired = true;
                            }
                            else if (allInstances.All(x => x.SendStatus == SendStatus.FailedMaxTimes) && series.SendStatus != SendStatus.FailedMaxTimes)
                            {
                                //  Update Series if all related instances are Sent and status is not already set
                                var fromContext = db.Series.Single(x => x.SeriesUID == series.SeriesUID);
                                fromContext.SendStatus = SendStatus.FailedMaxTimes;
                                dbUpdateRequired = true;
                            }
                            else if (allInstances.All(x => x.SendStatus == SendStatus.Failed) && series.SendStatus != SendStatus.Failed)
                            {
                                //  Update Series if all related instances are Sent and status is not already set
                                var fromContext = db.Series.Single(x => x.SeriesUID == series.SeriesUID);
                                fromContext.SendStatus = SendStatus.Failed;
                                dbUpdateRequired = true;
                            }
                        }
                        if (dbUpdateRequired)
                            db.SaveChanges();           //  To reflect changes for the checks that follow

                        var relatedSeries = db.Series.Where(x => x.Study.StudyUID == study.StudyUID);
                        if (relatedSeries.All(x => x.SendStatus == SendStatus.Sent) && study.SendStatus != SendStatus.Sent)
                        {
                            //  Update Study if all related Series are Sent and status is not already set
                            var fromContext = db.Studies.Single(x => x.StudyUID == study.StudyUID);
                            fromContext.SendStatus = SendStatus.Sent;
                            dbUpdateRequired = true;
                        }
                        else if (relatedSeries.All(x => x.SendStatus == SendStatus.FailedMaxTimes) && study.SendStatus != SendStatus.FailedMaxTimes)
                        {
                            //  Update Study if all related Series are Sent and status is not already set
                            var fromContext = db.Studies.Single(x => x.StudyUID == study.StudyUID);
                            fromContext.SendStatus = SendStatus.FailedMaxTimes;
                            dbUpdateRequired = true;
                        }
                        else if (relatedSeries.All(x => x.SendStatus == SendStatus.Failed) && study.SendStatus != SendStatus.Failed)
                        {
                            //  Update Study if all related Series are Sent and status is not already set
                            var fromContext = db.Studies.Single(x => x.StudyUID == study.StudyUID);
                            fromContext.SendStatus = SendStatus.Failed;
                            dbUpdateRequired = true;
                        }
                    }

                    if (dbUpdateRequired)
                        db.SaveChanges();

                    db.Database.Connection.Close();     //  Explicitly closing to try to free up SQL connection pool
                }
            }
            catch (Exception ex)
            {
                FormatExceptionLog(ex, "CheckAndUpdate");
                //  Swallow exception and carry on
            }
        }

        internal static bool IsPACSReachable(DicomAssociation assoc, RemoteAE remoteAE, TlsInitiator GetTLSStream)
        {
            try
            {
                if (DICOMEcho(assoc, remoteAE, GetTLSStream) == (int)StatusCodes.Success)
                    return true;
                else
                {
                    Log($"RemoteAE {remoteAE.CalledAET} not reachable at {remoteAE.IP}:{remoteAE.Port}");
                    return false;       //  Deem unreachable
                }
            }
            catch
            {
                //  Discard any exception and consider not reachable
                return false;
            }
        }

        static int DICOMEcho(DicomAssociation assoc, RemoteAE remoteAE, TlsInitiator GetTLSStream)
        {
            assoc.InitiateTls += GetTLSStream;

            assoc.Open(remoteAE.IP, remoteAE.Port, remoteAE.CallingAET, remoteAE.CalledAET);
            int status = assoc.Echo();

            //DebugLog($"C-Echo response received: 0x{status:X4}");
            return status;
        }

        internal static IEnumerable<string> TStoPropose(string originalTS, bool ForLocalPACS)
        {
            try
            {
                //  Add Original TSs first
                List<string> TSs = new List<string>() { originalTS };

                //  Workaround to list only the original TSs if Local Pacs already supports them, more changes of accepted = original
                if (ForLocalPACS && PACSPreferredTSs.Contains(originalTS))
                {
                    //DebugLog($"Only proposing Original TS {originalTS} to LocalPACs, assuming it is supported");
                    return TSs;
                }

                //  Making sure Default TS is included
                TSs.Add(TransferSyntaxes.ImplicitVRLittleEndian);

                TSs = TSs.Distinct().ToList();      //  Avoid duplicate if OrignalTS is ImplicitVRLittleEndian

                //DebugLog($"Proposed TSs:{Environment.NewLine}\t{string.Join($",{Environment.NewLine}\t", TSs)}");
                return TSs;
            }
            catch (Exception ex)
            {
                FormatExceptionLog(ex, "TStoPropose");
                //  Swallow exception and rethrow
                throw;
            }
        }

        // Break a list of items into chunks of a specific size
        internal static IEnumerable<T>[] CreateBatches<T>(IEnumerable<T> source, int chunkSize)
        {
            //  Smaller chunks if total items are less
            if (source.Count() <= InitConfig.MaxNumberOfSenderThreads)
                chunkSize = 1;      //  Minimum 1 item per batch
            else if (source.Count() < chunkSize * InitConfig.MaxNumberOfSenderThreads)
            {
                //  Adjust chunk size
                chunkSize = source.Count() / InitConfig.MaxNumberOfSenderThreads;
            }

            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / chunkSize)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToArray();
        }
        #endregion

        #region Database        
        public static PACSRouterDBContext GetDB()
        {
            return new PACSRouterDBContext();
        }

        const string DEFAULT_OUTPUTDIR = "Other";
        public static string GenerateStorageFilename(DicomDataSet ReceivedInstance, string StorageRootDirectory)
        {
            //  Build output file path
            string modality, studyDate, patientID, instance_GUID, storageDir;

            modality = ReceivedInstance[Keyword.Modality]?.Value as string ?? DEFAULT_OUTPUTDIR;
            studyDate = (ReceivedInstance[Keyword.StudyDate]?.Value as DateTime?)?.ToString("yyyyMMdd") ?? DEFAULT_OUTPUTDIR;
            patientID = ReceivedInstance[Keyword.PatientID]?.Value as string ?? DEFAULT_OUTPUTDIR;
            instance_GUID = (ReceivedInstance[Keyword.SOPInstanceUID]?.Value as string ?? DEFAULT_OUTPUTDIR) + "_" + Guid.NewGuid();            //     Add new GUID to ensure no duplicate filename are generated

            storageDir = Path.Combine(StorageRootDirectory, modality, studyDate, patientID);

            //  Check and create output directory
            if (!Directory.Exists(storageDir))
            {
                DebugLog($"\tCreating output directory: '{storageDir}'");
                Directory.CreateDirectory(storageDir);
            }

            return Path.Combine(storageDir, instance_GUID + ".dcm");
        }
        #endregion

        #region Logging and error handling
        internal static void Log(string msg)
        {
            DicomGlobal.Log(msg);
            DebugLog(msg);
        }
        internal static void DebugLogIf(bool Condition, string msg)
        {
#if DEBUG
            if (Condition)
                DebugLog(msg);
#endif
        }
        internal static void DebugLog(string msg)
        {
#if DEBUG
            Debug.WriteLine(msg);
            Console.WriteLine(msg);
#endif
        }
        internal static void FormatExceptionLog(Exception ex, string Msg)
        {
            bool DBOpenError = false;
            if (ex is EntityException && ex.Message.Contains("The underlying provider failed on Open"))
                DBOpenError = true;

            Log($"{Environment.NewLine}--------\t{Msg} exception!\t--------" +
                $"{Environment.NewLine}\t{ex.Message}" +
                $"{Environment.NewLine}{ex.StackTrace}");

            if (DBOpenError) Log(new string('*', 130) + Environment.NewLine);
        }
        #endregion

        #region Receiver helpers
        internal static void ForwardToLocalPACS(RemoteAE remoteAE, Instance instance)
        {
            //  Re-use Sender code
            DICOMSend(new[] { instance }, remoteAE, false /*  No need to refresh or save DB, just update instance which gets saved/inserted later */);
        }
        internal static void SplitName(string fullname, out string firstname, out string lastname)
        {
            string name;
            lastname = string.Empty;
            if (fullname.Contains("="))
            {
                int p = (fullname + "=").IndexOf("=");
                name = fullname.Substring(0, p - 1);
            }
            else
                name = fullname.Trim();

            firstname = name;

            string[] NameComponents;
            if (name.IndexOf("^") >= 0)
            {
                char[] charSeparators = new char[] { '^' };
                NameComponents = (name + "^^^^^").Split(charSeparators);
                lastname = NameComponents[0];
                firstname = NameComponents[1];
            }
            else if (name.IndexOf(",") >= 0)
            {
                char[] charSeparators = new char[] { ',' };
                NameComponents = (name + ",,,,,").Split(charSeparators);
                lastname = NameComponents[0];
                firstname = NameComponents[1];
            }
            lastname = lastname.Trim();
            firstname = firstname.Trim();
        }
        #endregion

        #region Routing Audit helpers
        internal static string HandleQuotes(string str)
        {
            return System.Text.RegularExpressions.Regex.Replace(str, "'", "''");
        }
        #endregion
    }
}
