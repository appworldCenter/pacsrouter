﻿using System.ServiceProcess;

namespace PACSRouter
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun = new ServiceBase[] { new PACSRouterService() };

#if false   //  Console switch only for Debugging
            //  Run as console application for debugging!
            Utils.DebugLog("Debugging ...\t\tHit any key to stop");
            PACSRouter router = new PACSRouter();
            router.Start();

            System.Console.ReadKey();
            router.Stop();
#else            
            ServiceBase.Run(ServicesToRun);
#endif
        }
    }
}
