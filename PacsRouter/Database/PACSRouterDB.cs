﻿using DicomObjects;
using DicomObjects.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using static PACSRouter.Utils;

namespace PACSRouter
{
    //  DB structure
    public class PACSRouterDBContext : DbContext
    {
        const string DatabaseName = "PACSRouter";
        public PACSRouterDBContext() : base(ProjectSettings.ConnectionString)
        {
            Database.SetInitializer(new CustomDBInitializer());
        }

        public DbSet<Study> Studies { get; set; }
        public DbSet<Series> Series { get; set; }
        public DbSet<Instance> Instances { get; set; }
        public DbSet<RemoteAE> RemoteAEs { get; set; }
        public DbSet<PreferredTS> PreferredTSs { get; set; }
        public DbSet<Configuration> Configurations { get; set; }
        public DbSet<ListeningPort> ListeningPorts { get; set; }
        public DbSet<RoutingRule> RoutingRules { get; set; }
        public DbSet<RoutingStatus> RoutingStatuses { get; set; }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //  Explicilty close underlying connection
                Database.Connection.Close();
            }
            base.Dispose(disposing);
        }
    }


    //  Table Definition
    public abstract class GenericTable
    {
        [Required]
        public SendStatus SendStatus { get; set; }
    }
    public class Study : GenericTable
    {
        //  Main Primary key
        [Required]
        [Key]
        [StringLength(64)]
        public string StudyUID { get; set; }

        [StringLength(16)]
        public string AccessionNumber { get; set; }

        [StringLength(16)]
        public string StudyID { get; set; }

        [StringLength(64)]
        public string ReferringPhysicianName { get; set; }

        [StringLength(64)]
        public string StudyDescription { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? StudyDate { get; set; }

        [StringLength(64)]
        public string PatientID { get; set; }

        [Required]
        [StringLength(250)]
        public string PatientName { get; set; }

        [StringLength(50)]
        public string PatientFirstName { get; set; }

        [StringLength(50)]
        public string PatientLastName { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? PatientDateOfBirth { get; set; }

        [StringLength(16)]
        public string PatientSex { get; set; }

        [Required]
        [Column(TypeName = "datetime2")]
        public DateTime DateReceived { get; set; }

        //  Building Study table entry for received instance but first check for existing
        public static Study CreateIfNecessary(DicomDataSet ReceivedInstance, DateTime NewTimeStamp, PACSRouterDBContext db)
        {
            /*
             * Wait if another thread has created a new entry and hasn't committed to DB yet
             * This is to avoid multiple new entries with the same new UID
             */
            int currentThreadID = Thread.CurrentThread.ManagedThreadId;
            string UID = ReceivedInstance.StudyUID;

            lock (newStudyUIDs)
            {
                if (!newStudyUIDs.ContainsKey(UID))
                    newStudyUIDs.TryAdd(UID, currentThreadID);    //  Add new UID for tracking
            }

            //      Wait till first thread finishes
            while (newStudyUIDs.ContainsKey(UID) && !newStudyUIDs.Contains(new KeyValuePair<string, int>(UID, currentThreadID)))
            {
                //DebugLog($"\t\tWaiting ({currentThreadID}) ...");
                //  Wait for the first thread to complete it
                Thread.Sleep(200);
            }

            var existingEntry = db.Studies.FirstOrDefault(x => x.StudyUID == ReceivedInstance.StudyUID);
            if (existingEntry != null)
            {
                newStudyUIDs.TryRemove(UID, out int v);

                existingEntry.SendStatus = SendStatus.Received; //  Reset
                existingEntry.DateReceived = NewTimeStamp;      //  Update received timestamp to newest
                return existingEntry;
            }
            else
            {
                string firstName, lastName;
                SplitName(ReceivedInstance.Name, out firstName, out lastName);

                Study newEntry = new Study
                {
                    StudyUID = ReceivedInstance.StudyUID,
                    AccessionNumber = ReceivedInstance.AccessionNumber,
                    StudyID = ReceivedInstance[Keyword.StudyID]?.Value as string,
                    ReferringPhysicianName = ReceivedInstance[Keyword.ReferringPhysicianName]?.Value as string,
                    StudyDescription = ReceivedInstance.StudyDescription,
                    StudyDate = ReceivedInstance[Keyword.StudyDate]?.Value as DateTime?,
                    PatientID = ReceivedInstance.PatientID,
                    PatientName = ReceivedInstance.Name,
                    PatientFirstName = firstName,
                    PatientLastName = lastName,
                    PatientDateOfBirth = ReceivedInstance[Keyword.PatientBirthDate]?.Value as DateTime?,
                    PatientSex = ReceivedInstance.Sex,
                    DateReceived = DateTime.Now
                };

                //  Insert new to DB
                db.Studies.Add(newEntry);

                return newEntry;
            }
        }
    }

    public class Series : GenericTable
    {
        //  Foreign key
        public virtual Study Study { get; set; }

        [StringLength(64)]
        [Required]
        [Key]
        public string SeriesUID { get; set; }

        [StringLength(64)]
        public string SeriesDescription { get; set; }

        [StringLength(10)]
        [Required]
        public string Modality { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? SeriesDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? SeriesTime { get; set; }

        [StringLength(200)]
        public string PerformingPhysicianName { get; set; }

        [StringLength(200)]
        public string RequestingPhysician { get; set; }

        [StringLength(200)]
        public string ScheduledPerformingPhysicianName { get; set; }

        [Required]
        [Column(TypeName = "datetime2")]
        public DateTime DateReceived { get; set; }

        //  Building Series table entry for received instance but first check for existing and Duplicate parent Study
        public static Series CreateIfNecessary(DicomDataSet ReceivedInstance, Study study, DateTime NewTimeStamp, PACSRouterDBContext db)
        {
            /*
             * Wait if another thread has created a new entry and hasn't committed to DB yet
             * This is to avoid multiple new entries with the same new UID
             */
            int currentThreadID = Thread.CurrentThread.ManagedThreadId;
            string UID = ReceivedInstance.SeriesUID;

            lock (newSeriesUIDs)
            {
                if (!newSeriesUIDs.ContainsKey(UID))
                    newSeriesUIDs.TryAdd(UID, currentThreadID);    //  Add new UID for tracking
            }

            //      Wait till first thread finishes
            while (newSeriesUIDs.ContainsKey(UID) && !newSeriesUIDs.Contains(new KeyValuePair<string, int>(UID, currentThreadID)))
            {
                //DebugLog($"\t\tWaiting ({currentThreadID})  - '{UID}'...");
                //  Wait for the first thread to complete it
                Thread.Sleep(200);
            }

            var existingEntry = db.Series.FirstOrDefault(x => x.SeriesUID == UID);
            if (existingEntry != null)
            {
                newSeriesUIDs.TryRemove(UID, out int v);

                existingEntry.SendStatus = SendStatus.Received; //  Reset
                existingEntry.DateReceived = NewTimeStamp;      //  Update received timestamp to newest
                return existingEntry;
            }
            else
            {
                Series newEntry = new Series
                {
                    SendStatus = SendStatus.Received,
                    Study = study,
                    SeriesUID = ReceivedInstance.SeriesUID,
                    SeriesDescription = ReceivedInstance.SeriesDescription,
                    Modality = ReceivedInstance[Keyword.Modality]?.Value as string,
                    SeriesDate = ReceivedInstance[Keyword.SeriesDate]?.Value as DateTime?,
                    SeriesTime = ReceivedInstance[Keyword.SeriesTime]?.Value as DateTime?,
                    PerformingPhysicianName = ReceivedInstance[Keyword.PerformingPhysicianName]?.Value as string,
                    RequestingPhysician = ReceivedInstance[Keyword.RequestingPhysician]?.Value as string,
                    ScheduledPerformingPhysicianName = ReceivedInstance[Keyword.ScheduledPerformingPhysicianName]?.Value as string,
                    DateReceived = DateTime.Now
                };

                //  Insert new to DB
                db.Series.Add(newEntry);
                return newEntry;
            }
        }
    }
    public class Instance : GenericTable
    {
        //  Foreign key
        public virtual Series Series { get; set; }

        public int RemoteAEID { get; set; }

        [Required]
        [Key]
        public int ID { get; set; }

        [StringLength(64)]
        [Required]
        public string StorageTS { get; set; }

        [StringLength(64)]
        [Required]
        public string InstanceUID { get; set; }

        [StringLength(64)]
        public string SOPClassUID { get; set; }

        [Required]
        [Column(TypeName = "datetime2")]
        public DateTime DateReceived { get; set; }

        [StringLength(500)]
        [Required]
        public string FileLocation { get; set; }

        public int? LastResult { get; set; }

        public string ErrorComment { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastAttempt { get; set; }

        [Required]
        public int NumberOfFailedAttempts { get; set; }

        [Required]
        public long SendPriority { get; set; }

        //  Building Instance table entry for received instance
        public Instance(DicomDataSet ReceivedInstance, Series FromSeries, RemoteAE remoteAE, string Filelocation, DateTime TimeStamp)
        {
            SendStatus = SendStatus.Received;
            Series = FromSeries;
            StorageTS = ReceivedInstance.OriginalTS;
            InstanceUID = ReceivedInstance.InstanceUID;
            SOPClassUID = ReceivedInstance.SOPClass;
            DateReceived = TimeStamp;
            SendPriority = TimeStamp.Ticks;
            FileLocation = Filelocation;
            RemoteAEID = remoteAE.ID;
            NumberOfFailedAttempts = 0;
        }
        public Instance() { }
    }

    public class Configuration
    {
        [Required]
        [Key]
        public int ID { get; set; }

        [StringLength(16)]
        public string ImplementationName { get; set; }

        [StringLength(64)]
        public string ImplementationUID { get; set; }

        [Required]
        public int CacheDays { get; set; }

        [Required]
        public string StorageRootDirectory { get; set; }

        [Required]
        public int MaxNumberOfRetries { get; set; }

        [Required]
        public int SenderTaskFrequency { get; set; }

        [Required]
        public int MaintenanceTaskFrequency { get; set; }

        [Required]
        public int MaxNumberOfSenderThreads { get; set; }
    }

    public class RemoteAE
    {
        [Required]
        [Key]
        public int ID { get; set; }

        [Required]
        public string IP { get; set; }

        [Required]
        public int Port { get; set; }

        [Required]
        public string CallingAET { get; set; }

        [Required]
        public string CalledAET { get; set; }

        [Required]
        public ConnectionType Encryption { get; set; }

        [Required]
        public bool AlwaysForward { get; set; }

        [Required]
        public bool Disable { get; set; }

        [StringLength(64)]
        public string DestinationName { get; set; }
    }

    public class PreferredTS
    {
        [Required]
        [Key]
        public int ID { get; set; }

        [Required]
        public string TransferSyntax { get; set; }
    }

    public class ListeningPort
    {
        [Required]
        [Key]
        public int ID { get; set; }

        [Required]
        public int PortNumber { get; set; }
    }

    public class RoutingRule
    {
        //  Foreign key
        public virtual RemoteAE RemoteNode { get; set; }

        [Required]
        [Key]
        public int ID { get; set; }

        [Required]
        public string ExistingValue { get; set; }

        [Required]
        public short Group { get; set; }
        [Required]
        public short Element { get; set; }

        public string Comments { get; set; }
    }

    public class RoutingStatus : GenericTable
    {
        //  Foreign key
        public virtual Study Study { get; set; }
        public virtual Series Series { get; set; }
        public virtual RemoteAE RemoteNode { get; set; }

        [Required]
        [Key]
        public int ID { get; set; }

        public bool Audited { get; set; }
    }
}