﻿using DicomObjects.DicomUIDs;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace PACSRouter
{
    internal class CustomDBInitializer : DropCreateDatabaseIfModelChanges<PACSRouterDBContext>
    {
        protected override void Seed(PACSRouterDBContext context)
        {
            //  Populate seed
            context.Configurations.Add(new Configuration
            {
                CacheDays = 90,
                ImplementationName = "PACSRouter",
                ImplementationUID = "1.2.826.0.1.3680043.10.367.20191231.1616.2",
                MaxNumberOfRetries = 200,
                StorageRootDirectory = @"P:\PACSRouter\dicom",
                MaxNumberOfSenderThreads = Environment.ProcessorCount,
                SenderTaskFrequency = 1,                // value in minutes
                MaintenanceTaskFrequency = 24 * 60,     // value in minutes
            });

            context.PreferredTSs.AddRange(new List<PreferredTS>
            {
                new PreferredTS{ TransferSyntax = TransferSyntaxes.JPEGLosslessFirstOrder },
                new PreferredTS{ TransferSyntax = TransferSyntaxes.JPEGLossless },
                new PreferredTS{ TransferSyntax = TransferSyntaxes.JPEG2000Lossless },
                new PreferredTS{ TransferSyntax = TransferSyntaxes.ImplicitVRLittleEndian },
                new PreferredTS{ TransferSyntax = TransferSyntaxes.ExplicitVRLittleEndian }
            });

            context.ListeningPorts.Add(new ListeningPort { PortNumber = 108 });

            context.RemoteAEs.AddRange(new List<RemoteAE> {
                new RemoteAE {
                    CalledAET = "TestSCU",
                    IP = "localhost",
                    Port = 108,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router",
                },
                new RemoteAE {
                    CalledAET = "LOCAL",
                    IP = "localhost",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "PLANTMRI",
                    IP = "18.214.76.136",
                    Port = 108,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "COOPERGW",
                    IP = "18.214.76.136",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "FTLAUD",
                    IP = "18.214.76.136",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "WS1RIS",
                    IP = "172.31.24.93",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "AWSULTRA",
                    IP = "18.214.76.136",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "barb_calling",
                    IP = "127.0.0.1",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "PACSRouterlocal",
                    IP = "127.0.0.1",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "PACSRouter",
                    IP = "107.203.192.185",
                    Port = 4010,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "COOPERGW",
                    IP = "107.203.192.185",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "ImageRequester",
                    IP = "127.0.0.1",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "FTLAUD",
                    IP = "96.77.35.81",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "CORAL_GW",
                    IP = "96.65.164.161",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "PLANTMRI",
                    IP = "50.192.146.209",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "PACS Router(Sec)",
                    IP = "107.203.192.185",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "PACS Router",
                    IP = "92.27.88.69",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "ITPACS-PRO",
                    IP = "127.0.0.1",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "PLANTMRI",
                    IP = "69.238.200.225",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "admin",
                    IP = "127.0.0.1",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "COOPERGW",
                    IP = "50.248.63.209",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "PACS Router",
                    IP = "50.248.63.209",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "PACS Router(Sec)",
                    IP = "50.248.63.209",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "THURBER2",
                    IP = "184.164.185.219",
                    Port = 4005,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "PACS Router(Sec)",
                    IP = "92.27.88.69",
                    Port = 104,
                    Disable = true,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "PACS Router",
                    IP = "109.224.193.3",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "LOCAL",
                    IP = "109.224.193.3",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "PACS Router",
                    IP = "90.240.233.56",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "PACS Router",
                    IP = "109.224.193.1",
                    Port = 104,
                    Disable = false,
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "WebPACSRouter",
                    IP = "127.0.0.1",
                    Port = 105,
                    Disable = false,
                    AlwaysForward = true,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                },
                new RemoteAE {
                    CalledAET = "DICOMMetaCK",
                    IP = "172.31.20.131",
                    Port = 4041,
                    Disable = false,
                    DestinationName = "Dicom Meta Data Checker",
                    AlwaysForward = false,
                    Encryption = ConnectionType.None,
                    CallingAET = "PACS Router"
                }
            });

            //  Remote AEs references for routing rules
            var ThurberHomeRemoteAE = new RemoteAE
            {
                CalledAET = "THURBER",
                IP = "108.235.157.180",
                Port = 4005,
                Disable = false,
                DestinationName = "Thurber Home",
                AlwaysForward = false,
                Encryption = ConnectionType.None,
                CallingAET = "PACS Router"
            };
            context.RemoteAEs.Add(ThurberHomeRemoteAE);

            var AlonsoRemoteAE = new RemoteAE
            {
                CalledAET = "READ_POM",
                IP = "10.70.245.140",
                Port = 104,
                Disable = false,
                DestinationName = "Alonso",
                AlwaysForward = false,
                Encryption = ConnectionType.None,
                CallingAET = "PACS Router"
            };
            context.RemoteAEs.Add(AlonsoRemoteAE);

            var HornsbyRemoteAE = new RemoteAE
            {
                CalledAET = "DR_HORNSBY",
                IP = "208.52.168.5",
                Port = 1112,
                Disable = false,
                DestinationName = "Hornsby Office",
                AlwaysForward = false,
                Encryption = ConnectionType.None,
                CallingAET = "PACS Router"
            };
            context.RemoteAEs.Add(HornsbyRemoteAE);

            var SanchezRemoteAE = new RemoteAE
            {
                CalledAET = "POM",
                IP = "18.214.76.136",
                Port = 11112,
                Disable = false,
                DestinationName = "Sanchez Cloud",
                AlwaysForward = false,
                Encryption = ConnectionType.None,
                CallingAET = "PACS Router"
            };
            context.RemoteAEs.Add(SanchezRemoteAE);

            var RobertRemoteAE = new RemoteAE
            {
                CalledAET = "RDMMP",
                IP = "216.242.34.134",
                Port = 4096,
                Disable = false,
                DestinationName = "Robert Martinez",
                AlwaysForward = false,
                Encryption = ConnectionType.None,
                CallingAET = "PACS Router"
            };
            context.RemoteAEs.Add(RobertRemoteAE);

            var PremierRemoteAE = new RemoteAE
            {
                CalledAET = "RADIUSPACS160",
                IP = "10.200.6.116",
                Port = 104,
                Disable = false,
                DestinationName = "Premier Radiology",
                AlwaysForward = false,
                Encryption = ConnectionType.None,
                CallingAET = "PACS Router"
            };
            context.RemoteAEs.Add(PremierRemoteAE);


            context.RoutingRules.AddRange(new List<RoutingRule> {
                new RoutingRule {
                    Group = 8,
                    Element = 4176,
                    ExistingValue = "Thurber",
                    RemoteNode = ThurberHomeRemoteAE,
                    Comments = "performing physician Thurber"
                },
                new RoutingRule {
                    Group = 50,
                    Element = 4146,
                    ExistingValue = "Thurber",
                    RemoteNode = ThurberHomeRemoteAE,
                    Comments = "requesting physician Thurber"
                },
                new RoutingRule {
                    Group = 8,
                    Element = 4192,
                    ExistingValue = "Thurber",
                    RemoteNode = ThurberHomeRemoteAE,
                    Comments = "Reading Physician Thurber"
                },
                new RoutingRule {
                    Group = 8,
                    Element = 4176,
                    ExistingValue = "Alonso",
                    RemoteNode = AlonsoRemoteAE,
                    Comments = "Reading Physician Alonso"
                },
                new RoutingRule {
                    Group = 50,
                    Element = 4146,
                    ExistingValue = "Alonso",
                    RemoteNode = AlonsoRemoteAE,
                    Comments = "performing physican Alonso"
                },
                new RoutingRule {
                    Group = 8,
                    Element = 4192,
                    ExistingValue = "Alonso",
                    RemoteNode = AlonsoRemoteAE,
                    Comments = "requesting physician Alonso"
                },
                new RoutingRule {
                    Group = 8,
                    Element = 4176,
                    ExistingValue = "Hornsby",
                    RemoteNode = HornsbyRemoteAE,
                    Comments = "performing physician Hornsby"
                },
                new RoutingRule {
                    Group = 50,
                    Element = 4146,
                    ExistingValue = "Hornsby",
                    RemoteNode = HornsbyRemoteAE,
                    Comments = "requesting physician Hornsby"
                },
                new RoutingRule {
                    Group = 8,
                    Element = 4192,
                    ExistingValue = "Hornsby",
                    RemoteNode = HornsbyRemoteAE,
                    Comments = "Reading Physician Hornsby"
                },
                new RoutingRule {
                    Group = 8,
                    Element = 4176,
                    ExistingValue = "SANCHEZ",
                    RemoteNode = SanchezRemoteAE,
                    Comments = "performing physician SANCHEZ"
                },
                new RoutingRule {
                    Group = 50,
                    Element = 4146,
                    ExistingValue = "SANCHEZ",
                    RemoteNode = SanchezRemoteAE,
                    Comments = "requesting physician SANCHEZ"
                },
                new RoutingRule {
                    Group = 8,
                    Element = 4192,
                    ExistingValue = "SANCHEZ",
                    RemoteNode = SanchezRemoteAE,
                    Comments = "Reading Physician Sanchez"
                },
                new RoutingRule {
                    Group = 8,
                    Element = 4176,
                    ExistingValue = "RMartinez",
                    RemoteNode = RobertRemoteAE,
                    Comments = "Performing Physician Robert Martinez"
                },
                new RoutingRule {
                    Group = 50,
                    Element = 4146,
                    ExistingValue = "RMartinez",
                    RemoteNode = RobertRemoteAE,
                    Comments = "Requesting Physician Robert Martinez"
                },
                new RoutingRule {
                    Group = 8,
                    Element = 4192,
                    ExistingValue = "RMartinez",
                    RemoteNode = RobertRemoteAE,
                    Comments = "Reading Physician Robert Martinez"
                },
                new RoutingRule {
                    Group = 8,
                    Element = 96,
                    ExistingValue = "MG",
                    RemoteNode = ThurberHomeRemoteAE,
                    Comments = "Modality to Thurber"
                },
                new RoutingRule {
                    Group = 8,
                    Element = 96,
                    ExistingValue = "SR",
                    RemoteNode = ThurberHomeRemoteAE,
                    Comments = "Modality to Thurber"
                },
                new RoutingRule {
                    Group = 8,
                    Element = 4176,
                    ExistingValue = "Premier",
                    RemoteNode = PremierRemoteAE,
                    Comments = "Performing Physician Premier Radiology"
                },
                new RoutingRule {
                    Group = 50,
                    Element = 4146,
                    ExistingValue = "Premier",
                    RemoteNode = PremierRemoteAE,
                    Comments = "Requesting Physician Premier Radiology"
                },
                new RoutingRule {
                    Group = 8,
                    Element = 4192,
                    ExistingValue = "Premier",
                    RemoteNode = PremierRemoteAE,
                    Comments = "Reading Physician Premier"
                },
                new RoutingRule {
                    Group = 8,
                    Element = 4192,
                    ExistingValue = "SANCHEZ^^^^",
                    RemoteNode = SanchezRemoteAE,
                    Comments = "Reading Physician Sanchez^^^^"
                },
                new RoutingRule {
                    Group = 8,
                    Element = 4192,
                    ExistingValue = "Premier^^^^",
                    RemoteNode = PremierRemoteAE,
                    Comments = "Reading Physician Premier^^^^"
                },
                new RoutingRule {
                    Group = 16,
                    Element = 16384,
                    ExistingValue = "Hornsby",
                    RemoteNode = HornsbyRemoteAE,
                    Comments = "Patient Comments Hornsby"
                },
                new RoutingRule {
                    Group = 16,
                    Element = 16384,
                    ExistingValue = "Sanchez",
                    RemoteNode = SanchezRemoteAE,
                    Comments = "Patient Comments Sanchez"
                },
                new RoutingRule {
                    Group = 16,
                    Element = 16384,
                    ExistingValue = "Alonso",
                    RemoteNode = AlonsoRemoteAE,
                    Comments = "Patient Comments Alonso"
                },
                new RoutingRule {
                    Group = 16,
                    Element = 16384,
                    ExistingValue = "RMartinez",
                    RemoteNode = RobertRemoteAE,
                    Comments = "Patient Comments Robert Martinez"
                },
                new RoutingRule {
                    Group = 16,
                    Element = 16384,
                    ExistingValue = "Premier",
                    RemoteNode = PremierRemoteAE,
                    Comments = "Patient Comments Premier"
                }
            });

            //  Initiate seed
            base.Seed(context);
        }
    }
}